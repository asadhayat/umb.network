const app = require('./src/app');
const PORT = process.env.APP_PORT;

const server = app.listen(PORT, () => {
    console.log(`App is running at http://localhost:${PORT} in ${app.get(
        'env'
      )} mode`);
});

process.on('unhandledRejection', reason => {
    console.error('Unhandled Rejection', reason);
});
  
process.on('uncaughtException', error => {
    console.error('Uncought Exception', error);
});
  