*This is a test project submitted to umb.network*

# Requirements
- **NEO4J**: App is using Neo4j database and is reuiqred to be up and running before starting the app.
- Provide Neo4j *bolt* url against *NEO4J_URL* entry in .env file
- Execute *npm run start* from command line to start the app

## Trust API
The App provides single API for calculating the trust between two ethereum addresses.

**GET** `/api/utils/trust?from=[from_addr]&to=[to_addr]`
###### Sample Response
```json
{
    "trust": 4
}
```