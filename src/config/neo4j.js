const neo4j = require('neo4j-driver')
require('dotenv').config();

const {
    NEO4J_URL,
    NEO4J_USERNAME,
    NEO4J_PASSWORD,
} = process.env;


const neo4jDriver = neo4j.driver(NEO4J_URL, neo4j.auth.basic(NEO4J_USERNAME, NEO4J_PASSWORD));

module.exports = {neo4jDriver};