require('dotenv').config();
const { neo4jDriver } = require('./../../config/neo4j');

const { NEO4J_DATABASE } = process.env;

async function getTrust(req, res) {


    try {
        const dbsession = neo4jDriver.session({ database: NEO4J_DATABASE });
        const from = req.query.from;
        const to = req.query.to;

        neoQuery = `
            MATCH (from:Address {address: '${from}'}), (to:Address {address: '${to}'})
            MATCH
            p=shortestPath((from)-[:Transaction*]->(to))
            RETURN p
        `;
        queryResp = await dbsession.run(neoQuery);
        try {
            if(queryResp.records[0]._fields[0]["length"]) {
                res.send({
                    trust: queryResp.records[0]._fields[0]["length"]

                });
                return
            }
        } catch (e) {
            res.send({trust: 0});
        }
        dbsession.close();
    } catch (e) {
        res.status(500);
        res.send(e.message);
    }
}



module.exports = {
    getTrust
}