const express = require('express');
const dotenv = require('dotenv');
const Utils = require('./utils.controller');
dotenv.config();

const router = express.Router();

router.get('/trust', Utils.getTrust);

module.exports = router;