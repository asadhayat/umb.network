const express = require('express');

const utilsRoutes = require('../controllers/utils/utils.routes');

const router = express.Router();




router.use('/utils', utilsRoutes);


module.exports = router;