const dotenv = require('dotenv');
const mongoose = require('mongoose');
dotenv.config();

async function init() {

    return mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
}

module.exports = {
    init
}