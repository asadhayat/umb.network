const express = require('express');
const assetService = require('./services/events.service');
const routes = require('./routes/routes');
const apiRoutes = require('./routes/api');
const { dbsession } = require('./config/neo4j');

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.set('PORT', process.env.APP_PORT);

app.use(express.json());

app.use('/api', apiRoutes)
app.use('/', routes);

module.exports = app;

assetService.start_BlockListner();