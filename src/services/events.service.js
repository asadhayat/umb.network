const Web3 = require('web3');
require('dotenv').config();
const { neo4jDriver } = require('./../config/neo4j');

const { INFURA_RPC_URL, NEO4J_DATABASE } = process.env;

const web3 = new Web3(process.env.INFURA_RPC_URL);

async function start_BlockListner() {
    web3.eth.subscribe('newBlockHeaders', function(error, result){
        if (error) {
            console.error('Error: ', error);
        }
    })
    .on("connected", function(subscriptionId){
        console.log('Connected. SubID: ', subscriptionId);
    })
    .on("data", function(blockHeader){
        web3.eth.getBlock(blockHeader.hash, true, (error, block) => {
            console.log(`Block ${block.number}`);
            processTransactions(block.transactions);            
        });
    })
    .on("error", console.error);
}

const processTransactions = async (transactions) => {
    console.log('Transactions', transactions.length);

    for(let transaction of transactions) {
        if (transaction.to == null) { continue; }
        await addTxToDB(transaction.from, transaction.to);
    }
}

const addTxToDB = async (from, to) => {
    const dbsession = neo4jDriver.session({database: NEO4J_DATABASE });
    await dbsession.run(`MERGE (a:Address {address: '${from}'})`);
    await dbsession.run(`MERGE (a:Address {address: '${to}'})`);

    const query = `
        MATCH (from:Address {address: '${from}'})
        MATCH (to:Address {address: '${to}'}) 
        MERGE (from)-[:Transaction]->(to)
    `;
    await dbsession.run(query);
    dbsession.close();
}

module.exports = {
    start_BlockListner
}